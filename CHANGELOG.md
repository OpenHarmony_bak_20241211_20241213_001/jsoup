#### 版本迭代
v2.0.0
  - 适配DevEco Studio 版本：4.1 Canary(4.1.3.317)，OpenHarmony SDK:API11 (4.1.0.36)
  - ArkTS新语法适配
  - globalThis模式使用GlobalContext单例代替

v1.0.2
  - 1. 适配DevEco Studio 3.1 Beta1版本

v1.0.1
  - 1. api8升级到api9
  - 2. 添加sanitize-html依赖库，完善jsoup功能
  - 3. 新增html转化为整洁的xhtml功能

v1.0.0

  已实现功能
  - 1. 从字符串解析HTML
  - 2. 提取CSS
  - 3. 解析自定义HTML标签
  - 4. 获取HTML属性
  - 5. 从字文件解析HTML
  - 6. HTML标签补充
  - 7. 从URL解析HTML
  - 8. HTML清除XSS